package main

import (
	"fmt"
	"testing"
)

func TestGetIntervalEstimate(t *testing.T) {
	fmt.Println("Testing getIntervalEstimate...")
	testInterval := 60000000000
	sampleCounts := []int{1,10,7,7,6,1}
	result, err := getIntervalEstimate(testInterval, sampleCounts)
	if err != nil {
		t.Errorf("getIntervalEstimate threw error: %s\n", err)
	}
	expected := 200000000000
	if expected != result {
		t.Errorf("Failed asserting that %d equals expected %d", result, expected)
	}

	sampleCounts = []int{}
	_, err = getIntervalEstimate(testInterval, sampleCounts)
	if err == nil {
		t.Errorf("Expected error but got nil: %v\n", err)
	}

	fmt.Printf("Done\n\n")
}
