package main

import (
	"github.com/coreos/bbolt"
	"time"
	"os"
	"fmt"
	"bufio"
	"io"
	"crypto/sha1"
	"encoding/json"
	"errors"
	"net/http"
	"github.com/vjeantet/jodaTime"
	"bytes"
	"reflect"
	"math/rand"
	"log"
)

const defaultIval = 30000000000 // Default download interval in nanoseconds (half minute)
const dbName = "webcams.db"
const queryTimeout = 1 // in seconds
const bucketName = "Webcams"
const imageFolder = "./images/"
const numPrevImageHashes = 10
const numNeededSampleCounts = 5 // Must be larger than 3

type Webcam struct {
	Hash 					[]byte
	Url 					string
	Ival					int
	PrevImageHashes			[][]byte // Save some image hashes
	CalculatingIdealIval 	bool
}

func (w *Webcam) addPrevImageHash(hash []byte) {
	if len(w.PrevImageHashes) >= numPrevImageHashes {
		w.PrevImageHashes = w.PrevImageHashes[1:] // Pop first hash
	}

	w.PrevImageHashes = append(w.PrevImageHashes, hash) // Push new hash to back
}

func (w *Webcam) imageIsNew(hash []byte) bool {
	for _, h := range w.PrevImageHashes {
		if reflect.DeepEqual(hash,h) {
			return false
		}
	}
	return true
}

func updateWebcam(webcam Webcam) {
	webcamBuf, err := json.Marshal(webcam)
	if err != nil {
		fmt.Errorf("Could not marshal webcam: %s\n", err)
	}

	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		err := b.Put(webcam.Hash, webcamBuf)
		return err
	})
}

func randomBetween(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max - min) + min
}

func createBucketIfNotExists(bucketName string) {
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		if err != nil {
			return fmt.Errorf("Could not create bucket: %s", err)
		}
		return nil
	})
}

// Checks our bucket for an entry with the given hash.
// Returns false and error if DB can't be opened.
func webcamExistsInDB(hash []byte) bool {
	exists := false

	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		v := b.Get(hash)

		exists = v != nil

		return nil
	})

	return exists
}

func updateDatabase() {
	fmt.Println("Updating database...")
	createBucketIfNotExists(bucketName)

	f, err := os.Open("urls")
	defer f.Close()
	if err != nil {
		fmt.Errorf("Could not open url file: %s\n", err)
	}

	reader := bufio.NewReader(f)
	for {
		buf, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Errorf("Could not read line: %s\n", err)
			}
		}

		url := string(buf)
		if url == "" {
			continue
		}

		h := sha1.New()
		h.Write(buf)
		hash := h.Sum(nil)

		if !webcamExistsInDB(hash) {
			webcam := Webcam{hash, url, defaultIval, [][]byte{}, true}
			updateWebcam(webcam)
		}
	}

	// TODO: Remove webcams that are in DB but not in url file

	fmt.Println("Done updating database")
}

func downloadImageEvery(webcam Webcam) {
	// Create samplecount slice
	sampleCounts := []int{}

	// Create image directory if not exists
	folderName := fmt.Sprintf("%x", webcam.Hash)
	folderPath := imageFolder + folderName
	_ = os.Mkdir(folderPath, 0776)

	for {
		logString := ""
		//log.Printf("%d: Downloading from %s\n", log.Ltime, webcam.Url)
		func() {
			filePath := folderPath + "/" + jodaTime.Format("YYYY-MM-dd-HH:mm:ss", time.Now()) + ".jpg"

			// Create the hash writer
			h := sha1.New()

			// Create copy buffer to store file contents in
			buf := bytes.Buffer{}

			// Create multiwriter for buffer and hash writer
			w := io.MultiWriter(&buf, h)

			// Download the image
			resp, err := http.Get(webcam.Url)
			if err != nil {
				fmt.Errorf("Could not get image file: %s\n", err)
				return
			}
			defer resp.Body.Close()

			// Write the body to buffer and hash writer
			_, err = io.Copy(w, resp.Body)
			if err != nil {
				fmt.Errorf("Could not write to multiwriter: %s\n", err)
			}

			// Get hash sum
			hash := h.Sum(nil)
			if err != nil {
				fmt.Errorf("Could not download file from %s: %v\n", webcam.Url, err)
			}

			if webcam.imageIsNew(hash) {
				logString += fmt.Sprintln("-----------------------------------------------------------")
				logString += fmt.Sprintf("Downloading new image from %s\n", webcam.Url)

				sampleCounts = append(sampleCounts, 1)

				logString += fmt.Sprintln(sampleCounts)

				// Create the file
				file, err := os.Create(filePath)
				defer file.Close()
				if err != nil {
					fmt.Errorf("Could not create file: %s\n", err)
				}

				// Write to the file
				_, err = io.Copy(file, &buf)
				if err != nil {
					fmt.Errorf("Could not write to file: %s\n", err)
				}

				// Add hash to webcam
				webcam.addPrevImageHash(hash)
				updateWebcam(webcam)
			}
		}()

		// Increment sampleCount
		if len(sampleCounts) == 0 {
			sampleCounts = append(sampleCounts, 1)
		} else {
			sampleCounts[len(sampleCounts)-1]++
		}

		if len(sampleCounts) >= numNeededSampleCounts && webcam.CalculatingIdealIval {
			intervalEstimate, err := getIntervalEstimate(webcam.Ival, sampleCounts)
			if err != nil {
				fmt.Errorf("Could not calculate interval estimate: %s\n", err)
				continue
			}
			if sumSliceInt(sampleCounts) < 2 * len(sampleCounts) {
				// Halve the estimate and keep calculating
				intervalEstimate = intervalEstimate / 2
				sampleCounts = []int{} // Start over
			} else {
				log.Printf("Interval estimate calculated: %d\n", intervalEstimate)

				webcam.Ival = intervalEstimate
				// Stop calculating
				webcam.CalculatingIdealIval = false
			}
			updateWebcam(webcam)
		}

		if len(logString) > 0 {
			logString += fmt.Sprintf("-------------------------------------------------------------------------------\n\n")
			log.Printf(logString)
		}

		time.Sleep(time.Duration(webcam.Ival))
	}
}

// Returns sum of the elements.
// Sum is zero if the slice is of zero length
func sumSliceInt(slice []int) int {
	s := 0
	for i := 0; i < len(slice); i++ {
		s += slice[i]
	}
	return s
}

func startWorker(webcam Webcam) {
	// Wait for a random time within the interval
	time.Sleep(time.Duration(randomBetween(0,webcam.Ival)))
	go downloadImageEvery(webcam)
}

// Calculate interval estimate
// Length of sampleCounts must be larger than 3
func getIntervalEstimate(testInterval int, sampleCounts []int) (int, error) {
	if len(sampleCounts) <= 3 {
		errorMsg := fmt.Sprintf("Samplecounts slice is too short (length %d not greater than 2)", sampleCounts)
		return 0, errors.New(errorMsg)
	}

	// We avoid using the two first counts for the sum to keep the estimate unbiased.
	// This is because some webcams alternate between two images.
	// The first count would then be very short and the second be too long

	// We also skip the last element as it will always be 1
	sumSampleCounts := 0
	for i := 2; i < len(sampleCounts) - 1; i++ {
		sumSampleCounts += sampleCounts[i]
	}
	idealInterval := testInterval / 2 / (len(sampleCounts) - 3) * sumSampleCounts
	return idealInterval, error(nil)
}

func startWorkers() {
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))

		b.ForEach(func(k, v []byte) error {
			webcam := Webcam{}
			err := json.Unmarshal(v, &webcam)
			if err != nil {
				fmt.Errorf("Could not unmarshal webcam: %s\n", err)
			}
			go startWorker(webcam)
			return nil
		})
		return nil
	})
}

var db *bolt.DB

func main() {
	var err error
	db, err = bolt.Open(dbName, 0600, &bolt.Options{Timeout: queryTimeout * time.Second})
	defer db.Close()
	if err != nil {
		fmt.Errorf("Could not open database %s\n", err)
	}

	updateDatabase()
	go startWorkers()
	select {} // Keep alive
}
